# Installing `LIBFLI` on Linux

This repository is to help installing `LIBFLI` on Linux.

`LIBFLI` is the **open source** Software Development Kit (SDK) provided by
[Finger Lakes Instrumentation (FLI)](https://www.flicamera.com) for their
products (except the Kepler camera).  The original code source of the SDK is
available [here](https://www.flicamera.com/software) and is copyrighted by
Finger Lakes Instrumentation (see [LICENSE.md](LICENSE.md) for the license).

The only changes to the original software consist in adding compilation,
linkage, and installation rules for Linux to the [`Makefile`](./Makefile).
Changes are in the new file [`Makefile.linux`](./Makefile.linux).

This software is released to open source in the hopes that it will be useful
for others.  Please don't make me regret it.

To install `LIBFLI`, clone this repository:

```.sh
git clone https://git-cral.univ-lyon1.fr/tao/libfli.git
```

Compile the libraries (static and shared versions):

```.sh
cd libfli
make
```

Install the libraries (static and shared versions) and the header files
respectively in directories `${PREFIX}/lib` and `${PREFIX}/include`:

```.sh
make PREFIX="$PREFIX" install
```
