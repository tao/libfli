# -*- Makefile -*-

#
#  The `-version-info` flag of LIBTOOL flag accepts an argument of the form
# `current:revision:age` where:
#
# - `current` uniquely identifies the latest interface inplemented, it can
#   only grow.
#
# - `revision` is the implementation number of `current`, that is the number
#    of bugs fixed...  This number shall reset to zero when `current` is
#    incremented.
#
# - `age` is used to specify the backward compatibility.  `age + 1` is the
#   number of interfaces implemented by `current`.  The library supports all
#   interfaces between `current − age` and `current`.
#
LIBFLI_CUR = 1
LIBFLI_REV = 104
LIBFLI_AGE = 0
#
# Here are a set of rules to help you update your library version
# information:
#
# 1. Start with version information of `0:0:0` for each LIBTOOL library.
#
# 2. Update the version information only immediately before a public release
#    of your software. More frequent updates are unnecessary, and only
#    guarantee that the current interface number gets larger faster.
#
# 3. If the library source code has changed at all since the last update,
#    then increment revision.
#
# 4. If any interfaces have been added, removed, or changed since the last
#    update, increment current, and set revision to 0.
#
# 5. If any interfaces have been added since the last public release, then
#    increment age.
#
# 6. If any interfaces have been removed or changed since the last public
#    release, then set age to 0.
#

LIBFLI_A=libfli.a
LIBFLI_SO=libfli.so
LIBFLI_SONAME=$(LIBFLI_SO).$(LIBFLI_CUR)
LIBFLI_SOTARGET=$(LIBFLI_SO).$(LIBFLI_CUR).$(LIBFLI_REV).$(LIBFLI_AGE)

# To build a shared library on Linux, you must compile with flag `-fpic` or
# `-fPIC` and link with options `-shared`, `-Wl,-soname,$(SONAME)` (where
# $(SONAME) is the name of the shared library with in the interface number),
# and perhaps with `-Wl,-rpath,$(RPATH)` (where $(RPATH) specifies the
# directories to search other related libraries).
CFLAGS += -fPIC

# Default target.
.PHONY: default
default: $(LIBFLI_A) $(LIBFLI_SO) $(LIBFLI_SONAME) $(LIBFLI_SOTARGET)
$(LIBFLI_SOTARGET): libfli.o $(ALLOBJ)
	$(CC) -shared -fPIC -Wl,-soname,$(LIBFLI_SONAME) -o $@ $^

$(LIBFLI_SONAME): $(LIBFLI_SOTARGET)
	ln -s $< $@

$(LIBFLI_SO): $(LIBFLI_SONAME)
	ln -s $< $@

install: $(LIBFLI_A) $(LIBFLI_SOTARGET)
	@if test -z "$(PREFIX)"; then \
	    echo >&2 "You must define installation directory with PREFIX=..."; \
	    return 1; \
	fi
	mkdir -p "$(PREFIX)/include"
	src="libfli.h"; dst="$(PREFIX)/include/$$src"; \
	rm -f "$$dst"; \
	cp -a "$$src" "$$dst"; \
	chmod 644 "$$dst"
	mkdir -p "$(PREFIX)/lib"
	src="$(LIBFLI_A)"; dst="$(PREFIX)/lib/$$src"; \
	rm -f "$$dst"; \
	cp -a "$$src" "$$dst"; \
	chmod 644 "$$dst"
	src="$(LIBFLI_SOTARGET)"; dst="$(PREFIX)/lib/$$src"; \
	rm -f "$$dst"; \
	cp -a "$$src" "$$dst"; \
	chmod 755 "$$dst"
	src="$(LIBFLI_SOTARGET)"; dst="$(PREFIX)/lib/$(LIBFLI_SONAME)"; \
	rm -f "$$dst"; \
	ln -s "$$src" "$$dst"
	src="$(LIBFLI_SONAME)"; dst="$(PREFIX)/lib/$(LIBFLI_SO)"; \
	rm -f "$$dst"; \
	ln -s "$$src" "$$dst"
